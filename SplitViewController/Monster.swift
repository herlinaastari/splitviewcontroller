//
//  Monster.swift
//  SplitViewController
//
//  Created by Herlina Astari on 28/09/19.
//  Copyright © 2019 Herlina Astari. All rights reserved.
//

import Foundation

import UIKit

enum Weapon {
    case blowgun, ninjaStar, fire, sword, smoke
    
    var name: String {
        switch self {
        case .blowgun:
            return "blowgun"
        case .fire:
            return "fire"
        case .ninjaStar:
            return "ninjastar"
        case .smoke:
            return "smoke"
        case .sword:
            return "sword"
        }
    }
}

class Monster {
    let name: String
    let description: String
    let iconName: String
    let weapon: Weapon
    
    init(name: String, description: String, iconName: String, weapon: Weapon) {
        self.name = name
        self.description = description
        self.iconName = iconName
        self.weapon = weapon
    }
    
    var icon: UIImage? {
        return UIImage(named: iconName)
    }
}
